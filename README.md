# es6play

### installed packages (npm install --save-dev xxx)

PS C:\Local\workspaces\js\es6play> npm list --depth=0
es6play@1.0.0 C:\Local\workspaces\js\es6play
+-- babel-core@6.26.0
+-- babel-preset-env@1.6.1
+-- babel-preset-es2015@6.24.1
+-- babel-preset-react@6.24.1
+-- babelify@8.0.0
+-- browserify@16.2.0
+-- gulp@3.9.1
+-- gulp-babel@7.0.1
`-- vinyl-source-stream@2.0.0

### installed packages globally (npm install -g xxx)

PS C:\Local\workspaces\js\es6play> npm list -g --depth=0
C:\Users\olehn\AppData\Roaming\npm
+-- gulp@3.9.1
`-- gulp-cli@2.0.1 - needed?