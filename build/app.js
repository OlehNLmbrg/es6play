
'use strict';

var m = new Map(); // ES6
m.set("hello", 42);
m.set(1, 11);
m.set(2, 22);
m.set("1", 1111);

console.log(m);

var s = new Set();
s.add("hello").add("R").add("G").add("B");

console.log(s);

// ES6 promises/fetch API

//fetch('https://devphp5.trendstop.com/ajax/members/main?gender=w', {
fetch('/index2.php', {
  headers: {
    'user-agent': 'Mozilla/4.0 MDN Example'
  },
  mode: 'cors'
}).then(function (response) {
  alert(response.status);
  if (response.status !== 200) {
    console.log('Looks like there was a problem. Status Code: ' + response.status);
    return;
  }

  // Examine the text in the response
  response.json().then(function (data) {
    console.log(data);
  });
  return;
}).catch(function (err) {
  console.log('Fetch Error :-S', err);
});