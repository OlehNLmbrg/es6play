'use strict';

var Greeting = React.createClass({
  displayName: 'Greeting',

  render: function render() {
    return React.createElement(
      'ul',
      null,
      React.createElement(
        'p',
        null,
        'Hello, Universe'
      ),
      React.createElement(
        'p',
        null,
        'Hello, sunshine'
      )
    );
  }
});

ReactDOM.render(React.createElement(Greeting, null), document.getElementById('greeting-div'));