
'use strict';
//
//var m = new Map();// ES6
//m.set("hello", 42);
//m.set(1, 11);
//m.set(2, 22);
//m.set("1", 1111);
//
//console.log(m);
//
//var s = new Set();
//s.add("hello").add("R").add("G").add("B");
//
//console.log(s);

// ES6 promises/fetch API

//fetch('https://devphp5.trendstop.com/ajax/members/main?gender=w', {
//fetch('/index2.php', {
//    headers: {
//      'user-agent': 'Mozilla/4.0 MDN Example'
//    },
//    mode: 'cors'
//  })
//  .then(
//    function(response) {
//        alert(response.status);
//      if (response.status !== 200) {
//        console.log('Looks like there was a problem. Status Code: ' +
//          response.status);
//        return;
//      }
//
//      // Examine the text in the response
//      response.json().then(function(data) {
//        console.log(data);
//      });
//      return;
//    }
//  )
//  .catch(function(err) {
//    console.log('Fetch Error :-S', err);
//  });

// trying reactJS within TSP
var AFL = {
    'host':null,
    'endpoints':{
        'local':{
            'story_nav':'/index_story.php?group_id=',
            'home_content_feed':'/index2.php'
        },
        'dev':{
            'home_content_feed':'https://devpremium.trendstop.com/ajax/members/main?gender=w'
        },
    },
    'TSP':{
        'bookmarks':{
            
        }
    }
};

;(function(){
    
var host = window.location.hostname;
let myRe1 = /localhost/g;
let myRe2 = /devpremium\.trendstop\.com/g;
if(myRe1.test(host)){
    AFL.host = 'local';
}else if(myRe2.test(host)){
    AFL.host = 'dev';
}

})();

console.log(AFL);