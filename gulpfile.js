/**
 * React.js Essentials
A fast-paced guide to designing and building scalable and maintainable web apps with React.js
Artemij Fedosejev
 * @type Module gulp|Module gulp
 */

var gulp = require('gulp');
var browserify = require('browserify');
//var babelify = require('babelify');
var source = require('vinyl-source-stream');

var babel = require('gulp-babel');

gulp.task('default', function() {
    console.log('gulp default. other options: TBC');
});

gulp.task('do', function () {
return browserify('./source/external_lib.js')
//.transform(babelify)
.bundle()
.pipe(source('external.js'))
.pipe(gulp.dest('./build/'));
});

// http://www.dotnetcurry.com/javascript/1293/transpile-es6-modules-es5-using-babel
gulp.task('transpile', function () {
return gulp.src(['./source/**/*.js'])
.pipe(babel({
  "presets": ["es2015","react"]
}))
.pipe(gulp.dest('./build/'));
});

